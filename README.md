# Tailwind JIT

Provides Just-in-Time compilation of Tailwind CSS for Drupal themes.

## Requirements

[Tailwind CSS](https://tailwindcss.com/) must be installed on the server.

- For prodcution environments, or if NodeJS is not available on your server, [download the standalone CLI build of Tailwind CSS](https://tailwindcss.com/blog/standalone-cli)
- If you are using composer on a Linux x64 system, you can download a [patched version of the Tailwind CLI including postcss-nested and postcss-import](https://packagist.org/packages/webtourismus/tailwindcss-cli) with composer:  
  `composer require webtourismus/tailwindcss-cli`
- If NodeJS is available on your server, [install Tailwind CSS using npm](https://tailwindcss.com/docs/installation). (Not recommended because the standalone CLI is faster than `npx`.)

## Installation

1. Install as you would normally install a contributed Drupal module.  
   For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
2. Make sure Tailwind CSS is available on your server.
3. In your `settings.php` file, create a variable named
   `$settings['tailwind_jit_executable']`
   containing the path to the Tailwind executable.
   - If you installed the standalone build, the path depends on the location
     to your downloaded binary and your OS:  
     You must either provide an absolute path like  
     `$settings['tailwind_jit_executable'] = '/usr/bin/tailwindcss-macos-arm64';`\
     or a path relative to the DRUPAL_ROOT directory (usually /web), e.g.  
     `$settings['tailwind_jit_executable'] = '../vendor/bin/tailwindcss'`;
   - If you installed the NodeJS version, the path most likely will be  
     `$settings['tailwind_jit_executable'] = 'npx tailwindcss'`;
4. After you have edited your `settings.php`, clear all Drupal caches:  
   Administration > Configuration > Development > Performance

## Configuration

1. Enable the module at Administration > Extend.
2. Go to your theme's settings and activate the compiler:  
   Administration > Appearance > Settings (of your theme) > Section "Tailwind CSS Just-in-time compilation"

## Performance disclaimer

This module is intented to be used with Drupal core's Internal Page Cache module. Using this module without cached responses from Internal Page Cache will negatively impact your site's performance!


## FAQ

**Q:** Can I use arbitrary values like `pt-[1.2345rem]`?  
**A:** Yes.

**Q:** Does it work with Views paging or Ajax requests?  
**A:** Yes.

**Q**: Can I use it with Layout builder or Paragraphs module?  
**A**: Yes, CSS classes rendered by Twig templates will be detected automatically. With the help of an extra module that provides an UI to inject CSS classes (like e.g. [Block class](https://www.drupal.org/project/block_class) or [Layout builder styles](https://www.drupal.org/project/layout_builder_styles) or [Style options module](https://www.drupal.org/project/style_options)) site builders can even style them on the fly in the admin UI .

**Q:** Can I use it with admin themes?  
**A:** Yes. I recommend the Field group module to easily finetune your UI
without creating your own subtheme and without any custom code. Want two
input fields in one line? Create a field group with those two fields below
and inject `grid grid-cols-2` on the parent container in the node form display.

**Q:** Is the compiled CSS cached?  
**A:** Yes, if "Internal Page Cache" is enabled and used. In practice this
usually means cached for anonymous users, but not cached for authenticated
users.

**Q:** Where do I put my `tailwind.config.js` and
how do I configure the template file paths (`content: []`)?  
**A:** You don't need a tailwind.config.js, the module will automatically
provide the current page's HTML as content to the compiler. If you need to
add additional custom config, use Tailwind's [@config](https://tailwindcss.com/docs/functions-and-directives#config)
directive inside your input CSS file.  
Note: If you use a custom config file, the content key will be ignored,
this key will always be set by the module.

**Q:** Can I use multiple CSS input files or
separate input files for individual Drupal libraries?  
**A:** No, not by library. By concept Tailwind CSS is designed to work with
one CSS input file. (This also means that Tailwind JIT is primarily meant to
be used by your theme, not by other modules). But is is possible to define a
separate input file for Ajax requests, e.g. one without Tailwind's preflight.
