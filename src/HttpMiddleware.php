<?php

declare(strict_types=1);


namespace Drupal\tailwind_jit;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Theme\ThemeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Interceps HTML responses to inject Tailwind generated CSS on demand.
 */
class HttpMiddleware implements HttpKernelInterface {

  /**
   * The wrapped HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * The tailwind_jit logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The wrapper for the commandline executable.
   *
   * @var \Drupal\tailwind_jit\Compiler
   */
  protected Compiler $compiler;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The constructor.
   */
  public function __construct(HttpKernelInterface $http_kernel,
                              LoggerInterface $logger,
                              Compiler $compiler,
                              ThemeManagerInterface $themeManager,
                              ModuleHandlerInterface $moduleHandler) {
    $this->httpKernel = $http_kernel;
    $this->logger = $logger;
    $this->compiler = $compiler;
    $this->themeManager = $themeManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritDoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, $catch = TRUE): Response {
    $response = $this->httpKernel->handle($request, $type, $catch);

    if ($response instanceof HtmlResponse) {
      return $this->processHtmlResponse($response);
    }

    if ($response instanceof AjaxResponse) {
      return $this->processAjaxResponse($response);
    }

    return $response;
  }

  /**
   * Injects Tailwind CSS into Ajax responses.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response used as content source for the Tailwind CSS compiler.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response including compiled Tailwind CSS styles
   */
  protected function processAjaxResponse(AjaxResponse $response): AjaxResponse {
    $currentTheme = $response->getAttachments()['drupalSettings']['ajaxPageState']['theme'] ?? '';
    if (!$currentTheme) {
      return $response;
    }
    if (!(theme_get_setting('tailwind_jit.compile_ajax_requests', $currentTheme) ?? FALSE)) {
      return $response;
    }
    $cssInputFile = $this->getInputFile($currentTheme, 'ajax_input_file');
    $content = $response->getContent();
    try {
      // Tailwind's parser doesn't detect classes with encoded slashes or adjacent to encoded quotes.
      $content = json_encode(json_decode($content, NULL, 512, JSON_THROW_ON_ERROR), JSON_UNESCAPED_SLASHES);
    } catch (\JsonException $exception) {
      return $response;
    }
    $configFile = NULL;
    if (!$cssInputFile) {
      $configFile = $this->moduleHandler->getModule('tailwind_jit')->getPath() . "/assets/tailwind.no-preflight.config.js";
    }
    $styleTag = $this->compiler->getCompiledCss($content, $cssInputFile, $configFile, $errorCode);
    if (empty($styleTag) || $errorCode) {
      return $response;
    }
    $response->addCommand(new AppendCommand('head', $styleTag), TRUE);
    $response->setData($response->getCommands());
    $response->headers->set('Content-Length', (string) strlen($response->getContent()), TRUE);
    return $response;
  }

  /**
   * Injects Tailwind CSS into HTML responses.
   *
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The response used as content source for the Tailwind CSS compiler.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The response including compiled Tailwind CSS styles
   */
  protected function processHtmlResponse(HtmlResponse $response): HtmlResponse {
    $currentTheme = $this->themeManager->getActiveTheme()->getName();
    if (!(theme_get_setting('tailwind_jit.compile_html_requests', $currentTheme) ?? FALSE)) {
      return $response;
    }
    $cssInputFile = $this->getInputFile($currentTheme, 'html_input_file');
    $content = $response->getContent();

    $errorCode = 0;
    $styleTag = $this->compiler->getCompiledCss($content, $cssInputFile, NULL, $errorCode);
    if (empty($styleTag) || $errorCode) {
      return $response;
    }

    $content = str_ireplace('</head>', $styleTag . '</head>', $content);
    $response->setContent($content);
    $response->headers->set('Content-Length', (string) strlen($response->getContent()), TRUE);
    return $response;
  }

  /**
   * Gets the CSS input file for the compiler, configured in theme settings.
   *
   * @param string $themeName
   *   Machine name of the theme.
   * @param string $settingName
   *   The input file setting of theme, by request type.
   *
   * @return string
   *   The full, absolute path to the CSS input file,
   *   or a blank string if none found
   */
  protected function getInputFile(string $themeName, string $settingName): string {
    $cssInputFileSetting = theme_get_setting("tailwind_jit.{$settingName}", $themeName);
    if (!$cssInputFileSetting) {
      return '';
    }
    $cssInputFile = realpath(DRUPAL_ROOT . '/' . $cssInputFileSetting);
    if (!$cssInputFile) {
      $this->logger->warning(
        'CSS input file "%filename" of theme "%theme" not found in local filesystem.',
        ['%filename' => $cssInputFileSetting, '%theme' => $themeName]
      );
      return '';
    }
    if (!str_starts_with($cssInputFile, DRUPAL_ROOT)) {
      $this->logger->warning(
        'Can not compile CSS input file "%filename" of theme "%theme" because it is outside of DRUPAL_ROOT.',
        ['%filename' => $cssInputFileSetting, '%theme' => $themeName]
      );
      return '';
    }
    return $cssInputFile;
  }

}
